import React from "react"
import { Row, Col, Button} from 'antd';
import "../assets/styles/headerblog.scss"
import { MailOutlined, FacebookOutlined, InstagramOutlined } from "@ant-design/icons"

const HeaderBlog = () => {
    return(
        <React.Fragment>
            <Row className="header-blog-contact">
                <Col className="header-blog-mail" flex={"90%"}>
                    <MailOutlined className="mail"/><p>Mail: info@hakita.id</p>
                </Col>
                <Col className="header-blog-icons">
                    <FacebookOutlined className="facebook" />
                    <InstagramOutlined className="instagram" />
                </Col>
            </Row>
            <Row className="header-blog">
                <Col flex={"50%"} className="header-blog-logo">
                    <a href="/blog"><img src={require("../assets/images/hakita.png")} alt="Hakita" /></a>
                </Col>
                <Col className="header-blog-navbar" flex={"auto"}>
                    <a href="/#home"><Button>Home</Button></a>
                    <a href="/#about"><Button>About</Button></a>
                    <a href="/#product-and-services"><Button>Services</Button></a>
                    <Button>Blog</Button>
                    <a href="/#contact"><Button>Contact</Button></a>
                </Col>
            </Row>
        </React.Fragment>
    )
}

export default HeaderBlog;