import React from "react"
import { Row, Col, Button} from 'antd';
import { UserOutlined } from '@ant-design/icons'
import "../assets/styles/headerhome.scss"

const HeaderHome = () => {
    return(
        <Row className="header">
            <Col flex={"30%"} className="header-logo">
                <a href="/"><img src={require("../assets/images/hakita.png")} alt="Hakita" /></a>
            </Col>
            <Col className="header-navbar" flex={"auto"}>
                <a href="#home"><Button>Home</Button></a>
                <a href="/#about"><Button>About</Button></a>
                <a href="#product-and-services"><Button>Services</Button></a>
                <Button>Pricing</Button>
                <a href="/blog"><Button>Blog</Button></a>
                <a href="#contact"><Button>Contact</Button></a>
                <a href="/signin"><Button className="header-login-btn" icon={<UserOutlined />}>Login</Button></a>
            </Col>
        </Row>
    )
}

export default HeaderHome;