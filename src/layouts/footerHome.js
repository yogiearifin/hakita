import React from "react"
import { Row, Col, Divider } from 'antd';
import "../assets/styles/footerhome.scss"
import { FacebookOutlined, InstagramOutlined } from "@ant-design/icons"

const FooterHome = () => {
    return(
        <React.Fragment>
            <Row className="footer">
                <Row className="footer-container">
                    <Col flex={"60%"} className="footer-about-us">
                        <h3>About us</h3>
                        <p>HAKITA adalah sebuah platform yang menjawab kebutuhan masyarakat Indonesia dalam membuat dan memproses dokumen legal yang aman dan terpercaya. Kami menggunakan teknologi otomasi untuk mempermudah proses pembuatan dokumen.</p>
                    </Col>
                    <Col flex={"auto"} className="footer-contact-us">
                        <h3>Contact Us</h3>
                        <p>Mail: info@hakita.com Alamat: Jl. Boulevard Barat Raya<br></br> No.27, RW. 9,Kelapa Gading Barat - Jakarta<br></br> Utara 14240</p>
                        <FacebookOutlined className="footer-facebook"/>
                        <InstagramOutlined className="footer-instagram" />
                    </Col>
                </Row>
            </Row>
                <Row className="footer-terms">
                    <Col className="footer-terms-item" flex={"70%"}>
                        <p>Syarat & Ketentuan</p>
                        <p>Kebijakan Privasi</p>
                        <p>Panduan Pembayaran</p>
                    </Col>
                    <Col flex={"auto"} className="footer-terms-copyright">
                        <p>© PT Legalindo Mitra Abadi, 2020</p>
                    </Col>
                </Row>
        </React.Fragment>
    )
}

export default FooterHome