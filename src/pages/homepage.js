import React from "react"
import HeaderHome from "../layouts/headerHome"
import FooterHome from "../layouts/footerHome"
import { Row, Col, Button} from 'antd';
import { MailOutlined, AimOutlined } from '@ant-design/icons'
import "../assets/styles/homepage.scss"
import HomeCards from "../components/HomeCards"
import ProductAndServices from "../components/ProductAndServices"
import Reading from "../components/Reading"

const Homepage = () => {
    return(
        <React.Fragment>
            <HeaderHome />
            <Row>
                <Col className="homepage-container">
                    <Col className="homepage-article" id="home">
                        <h1>URUSAN LEGAL JADI LEBIH<br></br> GAMPANG DENGAN HAKITA</h1>
                        <a href="/blog"><Button type="primary">Baca Artikel</Button></a>
                    </Col>
                </Col>
            </Row>
            <Row>
                <Col className="homepage-cards">
                    <HomeCards />
                </Col>
            </Row>
            <Row>
                <Col id="product-and-services">
                    <ProductAndServices />
                </Col>
            </Row>
            <Row>
                <Col className="homepage-fact">
                    <h1>APAKAH ANDA TAHU:</h1>
                    <h2>Kalau dari kasus tabrakan, Anda tidak<br></br> perlu memberi SIM atau KTP kepada<br></br> orang yang ditabrak</h2>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Reading />
                </Col>
            </Row>
            <h1 id="contact">Have a question?</h1>
            <Row className="homepage-question">
                <Col className="homepage-mail">
                    <MailOutlined className="homepage-mail-icon" />
                    <p>info@hakita.com</p>
                </Col>
                <Col className ="homepage-location">
                    <AimOutlined className="homepage-location-icon" />
                    <p>Jl. Boulevard Barat Raya No. 27,
                        Kelapa Gading Barat,
                        Jakarta Utara</p>
                </Col>
            </Row>
            <Row id="about">
                <FooterHome />
            </Row>
        </React.Fragment>
    )
}

export default Homepage;