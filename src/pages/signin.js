import React from "react"
import { Form, Input, Button, Checkbox, Row,Col } from 'antd';
import { UserOutlined, LockOutlined, GoogleOutlined, MailOutlined } from '@ant-design/icons';
import "../assets/styles/signin.scss"

const SignIn = () => {
    return(
        <React.Fragment>
            <Row className="sign-in-container">
                <Row className="sign-in">
                    <img src={require("../assets/images/hakita.png")} alt="Hakita" />
                    <Row className="sign-in-google">
                        <Button icon={<GoogleOutlined />}>Masuk dengan Google</Button>
                        <p>Atau gunakan email anda</p>
                    </Row>
                        <Form
                            name="normal_login"
                            className="login-form"
                        >
                        <Row className="sign-in-input">
                            <Row className="sign-in-email">
                                <Form.Item
                                    name="email"
                                    rules={[
                                        { required: true, message: 'Tolong masukan email anda!' },
                                        { type: 'email', message: 'Tolong masukan format email yang benar!',}]}
                                >
                                    <Input prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Email" />
                                </Form.Item>
                            </Row>
                            <Form.Item
                                name="password"
                                rules={[
                                    { required: true, message: 'Tolong masukan password anda!' },
                                    { min:8, message: 'Password minimal 8 karakter!',}]}
                            >
                                <Input
                                prefix={<LockOutlined className="site-form-item-icon" />}
                                type="password"
                                placeholder="Password"
                                />
                            </Form.Item>
                        </Row>
                    <Form.Item>
                        <Row className="sign-in-check">
                            <Col>
                                <Form.Item name="remember" valuePropName="checked" noStyle>
                                    <Checkbox>Ingat saya</Checkbox>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form.Item>
                    <Form.Item>
                        <Row className="sign-in-submit">
                            <Button type="primary" htmlType="submit" className="login-form-button">
                            Masuk
                            </Button>
                        </Row>
                        <Row className="sign-in-register">
                            Tak punya akun? <a href="/signup">daftar sekarang!</a>
                        </Row>
                    </Form.Item>
                    </Form>
                </Row>
            </Row>
    </React.Fragment>
    )
}

export default SignIn