import React from "react"
import { Form, Input, Button, Checkbox, Row,Col } from 'antd';
import { UserOutlined, LockOutlined, GoogleOutlined, MailOutlined } from '@ant-design/icons';
import "../assets/styles/signup.scss"

const SignUp = () => {
    return(
        <React.Fragment>
            <Row className="sign-up-container">
                <Row className="sign-up">
                    <img src={require("../assets/images/hakita.png")} alt="Hakita" />
                    <Row className="sign-up-google">
                        <Button icon={<GoogleOutlined />}>Daftar dengan Google</Button>
                        <p>Atau gunakan email anda</p>
                    </Row>
                        <Form
                            name="normal_login"
                            className="login-form"
                        >
                        <Row className="sign-up-input">
                            <Row className="sign-up-name">
                                <Form.Item
                                    name="name"
                                    rules={[
                                        { required: true, message: 'Tolong masukan nama anda!' }, 
                                    ]}
                                >
                                    <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Nama" />
                                </Form.Item>
                            </Row>
                            <Row className="sign-up-email">
                                <Form.Item
                                    name="email"
                                    rules={[
                                        { required: true, message: 'Tolong masukan email anda!' }, 
                                        { type: 'email', message: 'Tolong masukan format email yang benar!',}
                                    ]}
                                >
                                    <Input prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Email" />
                                </Form.Item>
                            </Row>
                            <Form.Item
                                name="password"
                                rules={[
                                    { required: true, message: 'Tolong masukan password anda!' },
                                    { min:8, message: 'Password minimal 8 karakter!',}]}
                            >
                                <Input
                                prefix={<LockOutlined className="site-form-item-icon" />}
                                type="password"
                                placeholder="Password"
                                />
                            </Form.Item>
                        </Row>
                    <Form.Item>
                        <Row className="sign-up-submit">
                            <Button type="primary" htmlType="submit" className="login-form-button">
                            Masuk
                            </Button>
                        </Row>
                        <Row className="sign-up-login">
                            Sudah punya akun? <a href="/signin">Login disini!</a>
                        </Row>
                    </Form.Item>
                    </Form>
                </Row>
            </Row>
    </React.Fragment>
    )
}

export default SignUp