import React from "react"
import HeaderBlog from "../layouts/headerBlog"
import {Helmet} from "react-helmet"
import { Row,Col, Input, Pagination } from "antd"
import "../assets/styles/blog.scss"
import BlogCards from "../components/BlogCards"
import PopularPost from "../components/PopularPost"
import FooterHome from "../layouts/footerHome"

const Blog = () => {
    const { Search } = Input;
    return(
        <React.Fragment>
            <HeaderBlog />
            <Helmet>
                <title>Blog Hakita - Punya pertanyaan soal hukum? Temukan jawabannya di blog HAKITA</title>
            </Helmet>
            <Row>
                <Col className="blog">
                    <h1>Blog</h1>
                </Col>
            </Row>
            <Row className="blog-content-container">
                <Col className="blog-content" flex={"80%"}>
                    <BlogCards />
                </Col>
                <Col className="blog-sidebar" flex={"auto"}>
                    <Search
                        placeholder="input search text"
                        style={{ width: 200 }}
                    />
                    <PopularPost />
                </Col>
            </Row>
            <Row>
                <Col className="blog-pagination">
                    <Pagination defaultCurrent={1} total={50} />
                </Col>
            </Row>
            <FooterHome />
        </React.Fragment>

    )
}

export default Blog