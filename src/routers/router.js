import React from "react"
import { Route, Switch } from "react-router-dom"
import Homepage from "../pages/homepage"
import Blog from "../pages/blog"
import SignIn from "../pages/signin"
import SignUp from "../pages/signup"

const Router = () => {
    return(
        <Switch>
            <Route path="/" component={Homepage} exact />
            <Route path="/blog" component={Blog} exact />
            <Route path="/signin" component={SignIn} exact />
            <Route path="/#about" component={Homepage} exact />
            <Route path="/#product-and-services" component={Homepage} exact />
            <Route path="/#contact" component={Homepage} exact />
            <Route path="/signup" component={SignUp} exact />
        </Switch>
    )
}

export default Router;