import { combineReducers } from "redux"
import cardsHome from "./cardsHome"
import product from "./product"
import reading from "./reading"
import cardsBlog from "./cardsBlog"

const rootReducers = combineReducers({
    cardsHome, product, reading, cardsBlog
})
export default rootReducers;
