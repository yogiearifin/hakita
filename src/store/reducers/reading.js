const initialState = {
    cards: [
        {
            id:1,
            title: "Konser Semasa COVID 19, Boleh?",
            date: "6/15/2020, 8:34:06 PM"
        },
        {
            id:2,
            title: "Instansi Pemerintah yang Tetap Buka semasa COVID 19",
            date: "6/14/2020, 8:11:02 PM"
        },
        {
            id:3,
            title: "Kegiatan Keagamaan Saat PSBB",
            date: "6/11/2020, 12:55:23 PM"
        },
    ]
}

const reading = (state = initialState, action) => {
    const { type } = action;
    switch (type) {
        default:
            return {
                ...state
            }
    }
}
export default reading;