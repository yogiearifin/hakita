const initialState = {
    cards: [
        {
            id:1,
            picture:require("../../assets/images/otomasi.png"),
            title: "Otomasi Dokumen",
            description:"Buat dokumen sudah tidak perlu ribet lagi. Dokumen yang biasa butuh waktu berminggu-minggu sekarang bisa selesai dalam hitungan menit dengan template dari HAKITA."
        },
        {
            id:2,
            picture:require("../../assets/images/manajemen.png"),
            title: "Sistem Manajemen Dokumen",
            description:"Kerja lebih efisien dengan HAKITA! Tidak hanya memantau ribuan dokumen yang Anda miliki, teknologi kami juga dapat memberi tahu Anda jika ada dokumen yang membutuhkan perhatian khusus lewat fitur reminder kami."
        },
    ]
}

const product = (state = initialState, action) => {
    const { type } = action;
    switch (type) {
        default:
            return {
                ...state
            }
    }
}
export default product;
