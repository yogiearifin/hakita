const initialState = {
    cards: [
        {
            id:1,
            picture:require("../../assets/images/needs.png"),
            title: "We Meet People’s Needs",
            description:"Kami sadar bahwa hukum masih dianggap sebagai sesuatu yang menakutkan dan belum terjangkau untuk semua golongan. Inilah mengapa kami ada; untuk melawan stigma dan menjawab segala kebutuhan legal Anda."
        },
        {
            id:2,
            picture:require("../../assets/images/protection.png"),
            title: "We Provide Protection",
            description:"Dibantu oleh praktisi hukum dengan pengalaman lebih dari 10 tahun, HAKITA memastikan bahwa Anda mendapatkan perlindungan hukum yang sesuai dengan hak Anda sebagai warga negara."
        },
        {
            id:3,
            picture:require("../../assets/images/trustworthy.png"),
            title: "We Are Trustworthy",
            description:"HAKITA hadir karena kami percaya bahwa hukum adalah hak semua orang. Semua yang kami lakukan di HAKITA selalu selaras dengan misi kami untuk menjadikan hukum sebagai sesuatu yang mudah dimengerti dan dapat diakses oleh semua orang."
        },
    ]
}

const cardsHome = (state = initialState, action) => {
    const { type } = action;
    switch (type) {
        default:
            return {
                ...state
            }
    }
}
export default cardsHome;
