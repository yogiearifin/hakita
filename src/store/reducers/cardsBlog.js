const initialState = {
    cards: [
        {
            id:1,
            picture:require("../../assets/images/konser.jpg"),
            title: "Konser Semasa COVID 19,Boleh?",
            date: "1 month ago in CONCERT",
            description:"Acara sosial seperti konser merupakan hal yang banyak ditunggu oleh kalangan muda di berbagai..."
        },
        {
            id:2,
            picture:require("../../assets/images/pemerintah.jpg"),
            title: "Instansi Pemerintah yang Tetap Buka semasa COVID 19",
            date: "1 month ago in covid 19",
            description:"Berbagai langkah telah dicoba untuk memerangi bahaya dan penyebaran COVID 19 yang kini..."
        },
    ]
}

const cardsBlog = (state = initialState, action) => {
    const { type } = action;
    switch (type) {
        default:
            return {
                ...state
            }
    }
}
export default cardsBlog;
