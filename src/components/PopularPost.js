import React from "react";
import { useSelector } from 'react-redux';
import { Row, Col } from 'antd';
import "../assets/styles/popularpost.scss"

const PopularPost = () => {
    const cards = useSelector(state => state.cardsBlog.cards)
    return(
        <>
        <h2>Popular Post</h2>
        <Row className="popularcards-container">
            {cards && cards.map(cards => 
            <Row className="popularcards-card-container">
                <Col flex={"30%"}>
                    <img src={cards.picture} alt={cards.id}/>
                </Col>
                <Col flex={"auto"} className="popular-card-item">            
                    <p>{cards.title}</p>
                    <p>{cards.date}</p>
                </Col>
            </Row>)}
        </Row>
        </>
    )

}

export default PopularPost