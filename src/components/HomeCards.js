import React from "react";
import { useSelector } from 'react-redux';
import { Row, Col } from 'antd';
import "../assets/styles/homecards.scss"

const HomeCards = () => {
    const cards = useSelector(state => state.cardsHome.cards)
    return(
        <Row className="homecards-container">
            {cards && cards.map(cards => 
            <Row className="homecards-card-container">
                <Col flex={"30%"}>
                    <img src={cards.picture} alt={cards.id}/>
                </Col>
                <Col flex={"auto"} className="card-item">            
                    <h3>{cards.title}</h3>
                    <p>{cards.description}</p>
                </Col>
            </Row>)}
        </Row>
    )

}

export default HomeCards