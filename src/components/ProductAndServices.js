import React from "react";
import { useSelector } from 'react-redux';
import { Row, Col, Button } from 'antd';
import "../assets/styles/productandservices.scss"

const ProductAndServices = () => {
    const service = useSelector(state => state.product.cards)
    return(
        <React.Fragment>
        <Row>
            <Col className="product-title">
                <h1>Product and Services</h1>
                <p>Apapun kebutuhan legal Anda, biar HAKITA yang urus.</p>
            </Col>
        </Row>
        <Row className="product-container">
            {service && service.map(cards =>
            <> 
                <Row className="product-card-container">
                    <Col className="product-card-icon">
                        <img src={cards.picture} alt={cards.id}/>
                    </Col>
                    <Col className="product-card-item">            
                        <h3>{cards.title}</h3>
                        <p>{cards.description}</p>
                        <Button>Learn More</Button>
                    </Col>
                </Row>
            </>
                )}
        </Row>
        </React.Fragment>
    )

}

export default ProductAndServices