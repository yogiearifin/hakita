import React from "react";
import { useSelector } from 'react-redux';
import { Row, Col, Divider } from 'antd';
import "../assets/styles/reading.scss"

const Reading = () => {
    const reading = useSelector(state => state.reading.cards)
    return(
        <React.Fragment>
            <Row className="reading">
                
                <h1>Love Reading, Keep Reading</h1>
                <p>Punya pertanyaan soal hukum? Temukan jawabannya di blog HAKITA.</p>
                <Row className="reading-container">
                    {reading && reading.map(cards => 
                    <Row className="reading-card-container">
                        <Col flex={"auto"} className="reading-card-item">   
                            <span>Blog</span>         
                            <h3>{cards.title}</h3>
                            <p>{cards.date}</p>
                            <p>Read more</p>
                            <Divider className="reading-divider" />
                        </Col>
                    </Row>)}
                </Row>
            </Row>
        </React.Fragment>
    )

}

export default Reading