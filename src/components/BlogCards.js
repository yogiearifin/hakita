import React from "react";
import { useSelector } from 'react-redux';
import { Row, Col } from 'antd';
import "../assets/styles/blogcards.scss"

const BlogCards = () => {
    const cards = useSelector(state => state.cardsBlog.cards)
    return(
        <Row className="blogcards-container">
            {cards && cards.map(cards => 
            <Row className="blogcards-card-container">
                <Col flex={"30%"}>
                    <img src={cards.picture} alt={cards.id}/>
                </Col>
                <Col flex={"auto"} className="blog-card-item">            
                    <h3>{cards.title}</h3>
                    <p>{cards.date}</p>
                    <p>{cards.description}</p>
                    <p>Continue Reading</p>
                </Col>
            </Row>)}
        </Row>
    )

}

export default BlogCards