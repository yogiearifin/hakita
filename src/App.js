import React from "react"
import { Provider } from "react-redux"
import { BrowserRouter } from "react-router-dom"
import store from "./store"
import Router from "./routers/router"


function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
      <React.Fragment>
        <Router />
      </React.Fragment>
      </BrowserRouter>
    </Provider>
    
  );
}

export default App;
